;; Hello World in Lisp

;; Classic Print Hello World
(write-line "Hello, World!")

; Defining Function for Hello World
(defun hello ()
    (write "Hello, World!") ())