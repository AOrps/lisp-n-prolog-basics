(defstruct person
    name
    phone_num
    height
    favorite_subject
)


( setq person1 (make-person 
    :name   "Joe Shmo"
    :phone_num "333"
    :height "5"
    :favorite_subject "Being a dipshit"
))

( setq person2 (make-person
    :name "Robert Worth"
    :phone_num "9340"
    :height "5'7"
    :favorite_subject "Math"
))


(write-line "Persons within Person Struct")
(write person1)
(write-line "")
(write person2)


; (defun get-name ()
;     "Gets the name of the user"
;     (write-line "Enter name please: ")
;     (setq name (read)) 
;     (cond  (= name "Andres"))
;     )