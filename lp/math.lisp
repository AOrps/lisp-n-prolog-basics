;; Do some math in lisp

(setq x ( +1 2 4 7))

(print x)


; Arithmetic Operations + Variable Definition
(defvar x (+ 7 12 15))

; Type of variable that x is and the value of x
(print (type-of x))
(print x)
