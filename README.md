# lisp-n-prolog-basics

Some basic programming in both Lisp and Prolog

| Directory | Language
| :----- | :-----
| [lp](lp/) | Lisp
| [pl](pl/) | Prolog

---

## Lisp


```lisp
;; Simple "Hello, World!"
(write-line "Hello world")
```
### To Run
```sh
clisp <filename>.lisp
```

---

## Prolog
```prolog
% Simple "Hello, World!"
hello_world :- write('hello world').
```

### To Run
- Everything must be in ran in the swi-prolog interpreter

```sh
# To "speed up" the loading, use -s
swipl -s <filename>.pl
```
Otherwise be sure to load your file in swipl.